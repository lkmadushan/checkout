import Vue from 'vue'
import store from './store'
import router from './router'
import App from './components/App'
import { sync } from 'vuex-router-sync'
import '../node_modules/vuetify/src/stylus/app.styl'
import './plugins'
import './filters'

sync(store, router)

Vue.config.productionTip = false

new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app')
