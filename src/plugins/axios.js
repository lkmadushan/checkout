import Vue from 'vue'
import axios from 'axios'

const instance = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
})

Vue.prototype.$http = instance

export default instance
