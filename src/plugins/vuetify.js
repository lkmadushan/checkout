import Vue from 'vue'
import { Vuetify, VApp, VBtn, VIcon, VGrid, transitions } from 'vuetify'

Vue.use(Vuetify, {
    components: {
        VApp,
        VBtn,
        VIcon,
        VGrid,
        transitions,
    },
    theme: {
        primary: '#233FC9',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#ee44aa',
        success: '#4CAF50',
        warning: '#FFC107',
    },
})
