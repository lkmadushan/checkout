import Vue from 'vue'

Vue.filter('pound', function(value) {
    if (!value) return ''
    return `£${value.toFixed(2)}`
})

Vue.filter('percent', function(value) {
    if (!value) return ''
    return `${value.toString()}%`
})
