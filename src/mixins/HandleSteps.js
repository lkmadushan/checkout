import { mapGetters } from 'vuex'
import AddTerm from './../components/AddTerm'
import AddDomain from './../components/AddDomain'
import AddAccount from './../components/AddAccount'
import AddBilling from './../components/AddBilling'
import AddPayment from './../components/AddPayment'
import AddCustomize from './../components/AddCustomize'

export default {
    computed: {
        ...mapGetters(['addons']),

        steps() {
            return [
                {
                    title: 'Account',
                    component: AddAccount,
                    next: 'AddDomain',
                    visible: true,
                },
                {
                    title: 'Domain Name',
                    component: AddDomain,
                    next: 'AddTerm',
                    visible: true,
                },
                {
                    title: 'Term',
                    component: AddTerm,
                    next: 'AddCustomize',
                    visible: true,
                },
                {
                    title: 'Customize',
                    component: AddCustomize,
                    next: 'AddBilling',
                    visible: !!this.addons.length,
                },
                {
                    title: 'Billing',
                    component: AddBilling,
                    next: 'AddPayment',
                    visible: true,
                },
                {
                    title: 'Payment',
                    component: AddPayment,
                    next: null,
                    visible: true,
                },
            ]
        },
    },
}
