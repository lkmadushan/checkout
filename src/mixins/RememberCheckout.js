const EXPIRES = parseInt(process.env.VUE_APP_COOKIE_EXPIRES)

export default {
    data: () => ({
        checkout: {
            name: '',
            type: '',
            email: '',
            town: '',
            country: 'GB',
            county: '',
            postcode: '',
            address1: '',
            address2: '',
            companyName: '',
            companyNumber: '',
            charityName: '',
            charityNumber: '',
            vatNumber: '',
            addons: [],
            domains: [],
            term_id: '',
            payment_method: 'bacs',
            card_name: '',
            card_number: '',
            card_expiry: '',
            card_cvv: '',
        },
    }),

    methods: {
        persistCheckout(payload) {
            const { uuid } = this.getCheckoutCookie()

            return this.$http.put(`checkout/${uuid}`, payload)
        },

        hasCheckoutCookie() {
            const cookie = this.$cookie.get('checkout')

            return cookie && cookie.length > 2
        },

        getCheckoutCookie() {
            return JSON.parse(this.$cookie.get('checkout'))
        },

        setCheckoutCookie(data, expires = 7) {
            this.$cookie.set('checkout', JSON.stringify(data), { expires })
        },

        async setNewCheckoutCookieFromServer() {
            this.setCheckoutCookie(await this.fetchServerCookie(), EXPIRES)
        },

        async setCheckoutCookieIfNotExists() {
            if (this.hasCheckoutCookie()) return

            this.setNewCheckoutCookieFromServer()
        },

        async fetchServerCookie() {
            const { data } = await this.$http.post('checkout')

            return data
        },

        async fetchCheckoutIfCookieExists() {
            if (!this.hasCheckoutCookie()) return

            const { uuid } = this.getCheckoutCookie()
            const { data } = await this.$http.get(`checkout/${uuid}`)

            if (data) {
                this.checkout = data
            } else {
                this.setNewCheckoutCookieFromServer()
            }
        },
    },
}
