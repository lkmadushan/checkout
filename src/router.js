import Vue from 'vue'
import VueRouter from 'vue-router'
import CheckoutView from './views/Checkout'

Vue.use(VueRouter)

const routes = [
    {
        name: 'checkout',
        component: CheckoutView,
        path: '/:productId',
    },
]

const baseUri = process.env.VUE_APP_PUBLIC_PATH

export default new VueRouter({
    base: baseUri,
    routes,
    mode: 'history',
})
