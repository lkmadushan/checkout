import Vue from 'vue'
import Vuex from 'vuex'
import axios from './plugins/axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        product: {
            specs: [],
            terms: [],
            addons: [],
        },
    },

    getters: {
        specs({ product }) {
            return product.specs.map(spec => {
                return {
                    title: spec.name.replace('%v', spec.value),
                    ...spec,
                }
            })
        },

        terms({ product }) {
            return product.terms
        },

        addons({ product }) {
            return product.addons
        },
    },

    mutations: {
        setProduct(state, product) {
            state.product = product
        },
    },

    actions: {
        async fetchProduct({ commit, state }, productId = null) {
            productId = productId || state.route.params.productId

            const { data } = await axios.get(`product/${productId}`)

            data && commit('setProduct', data)
        },
    },
})
