export function debounce(func, wait, immediate) {
    let timeout
    return function(...args) {
        clearTimeout(timeout)
        timeout = setTimeout(() => {
            timeout = null
            if (!immediate) func.apply(this, args)
        }, wait)
        if (immediate && !timeout) func.apply(this, [...args])
    }
}

export function upperFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
}
